﻿using EnDeCruption.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnDeCruption.Tests.Models
{
    public class MockFileWorker : IFileWorker
    {
        private string data;

        public MockFileWorker(string data)
        {
            this.data = data;
        }

        public string Read()
        {
            return data;
        }

        public void Write(string text)
        {
            data = text;
        }

        public void Dispose()
        {
            data = "";
        }
    }
}
