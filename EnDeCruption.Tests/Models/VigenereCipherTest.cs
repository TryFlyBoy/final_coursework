﻿using System;
using EnDeCruption.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EnDeCruption.Tests.Models
{
    [TestClass]
    public class VigenereCipherTest
    {
        [TestMethod]
        public void IsInitCorrect()
        {
            // Arrange
            string data = "шифротекст";
            var fileWorker = new MockFileWorker(data);

            // Act
            var crypt = new VigenereCipher(fileWorker, "шифроключ");

            // Assert
            Assert.AreEqual(data, crypt.Source);
        }
    }
}
