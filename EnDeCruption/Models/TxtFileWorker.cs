﻿using System;
using System.IO;

namespace EnDeCruption.Models
{
    public class TxtFileWorker : IFileWorker
    {
        private string path;

        public TxtFileWorker(string path)
        {
            this.path = path;
        }

        public string Read()
        {
            string text;

            using (StreamReader sr = new StreamReader(path))
                text = sr.ReadToEnd();

            return text;
        }

        public void Write(string text)
        {
            using (StreamWriter sw = new StreamWriter(path, false))
                sw.Write(text);
        }

        public void Dispose()
        {
            
        }
    }
}