﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace EnDeCruption.Models
{
    public class VigenereCipher : IDisposable
    {
        private IFileWorker fileWorker;
        private string key;
        public string Source { get; }
        public string Result { get; private set; }

        private string alph;
        private string[] dictionary; // таблица для шифрования
        private string newInput; // строка, содержащая только символы для шифрования (нужная для реализации алгоритма)
        private string newKey; // строка из ключей длины newInput (нужная для реализации алгоритма)
        
        public VigenereCipher(IFileWorker fileWorker, string key)
        {
            this.fileWorker = fileWorker;
            this.key = key.ToLower();
            
            Source = fileWorker.Read().ToLower();

            alph = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
            dictionary = new string[alph.Length];
            newInput = string.Empty; 
            newKey = string.Empty;

            // Создание newInput
            foreach (var x in Source)
                if (alph.Contains(x))
                    newInput += x;

            // создание newKey
            for (int i = 0; i < newInput.Length / key.Length; i++)
                newKey += key;

            for (int i = 0; i < newInput.Length - key.Length * (newInput.Length / key.Length); i++)
                newKey += key[i];

            // Заполнение dictionary
            for (int i = 0; i < dictionary.Length; i++)
                dictionary[i] = alph.Substring(i) + alph.Substring(0, i);
        }

        public VigenereCipher Encrypt()
        {
            int counter = 0;

            var res = new StringBuilder();

            for (int i = 0; i < Source.Length; i++)
            {
                if (alph.Contains(Source[i]))
                    res.Append(dictionary[alph.IndexOf(Source[i])][alph.IndexOf(newKey[counter++])]);
                else
                    res.Append(Source[i]);
            }

            Result = res.ToString();
            return this;
        }

        public VigenereCipher Decrypt()
        {
            int counter = 0;

            var res = new StringBuilder();

            for (int i = 0; i < Source.Length; i++)
            {
                if (alph.Contains(Source[i]))
                    res.Append(alph[dictionary[alph.IndexOf(newKey[counter++])].IndexOf(Source[i])]);
                else
                    res.Append(Source[i]);
            }

            Result = res.ToString();
            return this;
        }

        public void Save()
        {
            fileWorker.Write(Result);
        }

        public void Dispose()
        {
            if (fileWorker == null)
                return;

            fileWorker.Dispose();
        }
    }
}