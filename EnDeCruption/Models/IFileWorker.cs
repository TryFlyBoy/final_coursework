﻿using System;

namespace EnDeCruption.Models
{
    public interface IFileWorker : IDisposable
    {
        string Read();
        void Write(string text);
    }
}
