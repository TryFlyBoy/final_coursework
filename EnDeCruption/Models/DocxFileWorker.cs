﻿using System;
using System.Text;
using Microsoft.Office.Interop.Word;

namespace EnDeCruption.Models
{
    public class DocxFileWorker : IFileWorker
    {
        private Application application;
        private Document document;
        private string path;

        private bool isDisposed = false;

        public DocxFileWorker(string path)
        {
            this.path = path;

            try
            {
                application = new Application();
                application.Documents.Open(path);
                document = application.ActiveDocument;
            }
            catch
            {
                throw new Exception("Не удалось открыть документ!");
            }
        }

        public string Read()
        {
            var text = new StringBuilder();
            foreach (Paragraph par in document.Paragraphs)
                text.Append(par.Range.Text);
            
            return text.ToString();
        }

        public void Write(string text)
        {
            foreach (Paragraph par in document.Paragraphs)
                par.Range.Text = string.Empty;

            if (document.Paragraphs.Count == 0)
                document.Paragraphs.Add();

            document.Paragraphs[1].Range.Text = text;
        }

        public void Dispose()
        {
            if (isDisposed)
                return;

            if (document != null)
                document.Close();
            if (application != null)
                application.Quit();

            isDisposed = true;
        }
    }
}