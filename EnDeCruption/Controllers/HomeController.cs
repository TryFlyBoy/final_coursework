﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Text;
using Microsoft.Office.Interop.Word;
using EnDeCruption.Models;

namespace EnDeCruption.Controllers
{
    public class HomeController : Controller
    {
        string path;
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        public bool Getfile(HttpPostedFileBase upload)
        {
            if (upload != null)
            {
                var tempFiles = Directory.GetFiles(Server.MapPath("~/FilesIn"));
                for(int i = 0; i < tempFiles.Length; i++)
                    System.IO.File.Delete(tempFiles[i]);

                // получаем имя файла
                string fileName = Path.GetFileName(upload.FileName);
                // сохраняем файл в папку FilesIn в проекте
                path = Server.MapPath($"~/FilesIn/{fileName}");
                if (Path.GetExtension(path) == ".txt" || Path.GetExtension(path) == ".docx")
                {
                    upload.SaveAs(path);
                    return true;
                }
            }

            return false;
        }
        
        [HttpPost]
        public FileResult UploadDecryption(HttpPostedFileBase upload, string key)
        {
            return UploadCryption(upload, key, false);
        }

        [HttpPost]
        public FileResult UploadEncryption(HttpPostedFileBase upload, string key)
        {
            return UploadCryption(upload, key, true);
        }

        private FileResult UploadCryption(HttpPostedFileBase upload, string key, bool isEncryption)
        {
            if (Getfile(upload))
            {
                Сryption(isEncryption, key);
                string file_type = string.Empty;
                string file_name = string.Empty;
                if (Path.GetExtension(path) == ".txt")
                {
                    file_name = "Result.txt";
                    file_type = "application/txt";
                }
                else if (Path.GetExtension(path) == ".docx")
                {
                    file_name = "Result.docx";
                    file_type = "application/docx";
                }
                return File(path, file_type, file_name);
            }
            else
            {
                return File(Server.MapPath("~/error.txt"), "application / txt", "Result.txt");
            }
        }

        // ----------------------------------------------------------------------------------------------
        /// <summary>
        /// Метод для шифрования / дешифрования информации
        /// </summary>
        /// <param name="isEncryption">шифрование (true) / дешифрование (false)</param>
        /// <param name="key">ключ шифрования</param>
        public void Сryption(bool isEncryption, string key)
        {
            VigenereCipher crypt;

            if (Path.GetExtension(path) == ".txt")
                crypt = new VigenereCipher(new TxtFileWorker(path), key);
            else if (Path.GetExtension(path) == ".docx")
                crypt = new VigenereCipher(new DocxFileWorker(path), key);
            else
                throw new NotImplementedException("Данный функционал еще не реализован!");

            using (crypt)
            {
                if (isEncryption) // шифрование
                    crypt.Encrypt().Save();
                else // дешифрование
                    crypt.Decrypt().Save();
            }
        }
    }
}