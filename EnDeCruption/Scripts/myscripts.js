﻿function disp(div) {
    let elem = document.getElementById('div_encrypt');
    if (div != elem) {
        elem.style.display = "none";
    }
    else {
        elem = document.getElementById('div_decrypt');
        elem.style.display = "none";
    } 

    if (div.style.display == "none") {
        div.style.display = "block";
    } else {
        div.style.display = "none";
    }
}

$('#input_key').on('keypress', function () {
    var that = this;

    setTimeout(function () {
        var res = /[^а-я ]/g.exec(that.value);
        console.log(res);
        that.value = that.value.replace(res, '');
    }, 0);
});

function ExternalError(div) {
    let elem = document.getElementById('MoreInfo');
    if (elem.style.display == "none") {
        elem.style.display = "block";
    } else {
        elem.style.display = "none";
    }
}